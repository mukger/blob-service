package handlers

import (
	"blob/internal/service/handlers/requests"
	"blob/resources"
	"gitlab.com/distributed_lab/ape"
	"gitlab.com/distributed_lab/ape/problems"
	"net/http"
)

func GetBlobs(w http.ResponseWriter, r *http.Request) {
	request, err := requests.NewGetBlobListRequest(r)
	if err != nil {
		ape.RenderErr(w, problems.BadRequest(err)...)
		return
	}

	tempBlobs := BlobQ(r)

	tempBlobs.Page(request.OffsetPageParams)
	if len(request.FilterOwner) > 0 {
		tempBlobs.FilterByOwner(request.FilterOwner...)
	}

	if err != nil {
		Log(r).WithError(err).Error("failed to get blobs")
		ape.Render(w, problems.InternalError())
		return
	}

	blobs, err := tempBlobs.Select()

	blobCreatorList := make([]resources.Relation, len(blobs))
	blobsList := make([]resources.Blob, len(blobs))
	for i, b := range blobs {
		tempKey := resources.Key{ID: blobs[i].Creator, Type: "account"}
		blobCreatorList[i] = resources.Relation{Data: &tempKey}
		blobsList[i] = newBlobModel(b, &blobCreatorList[i])
	}

	response := resources.BlobListResponse{
		Data:  blobsList,
		Links: GetOffsetLinks(r, request.OffsetPageParams),
	}

	ape.Render(w, response)
}
