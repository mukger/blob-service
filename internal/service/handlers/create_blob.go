package handlers

import (
	"blob/internal/data"
	"blob/internal/service/handlers/requests"
	"blob/resources"
	"encoding/json"
	"fmt"
	"gitlab.com/distributed_lab/ape"
	"gitlab.com/distributed_lab/ape/problems"
	"net/http"
)

func CreateBlob(w http.ResponseWriter, r *http.Request) {
	request, err := requests.NewCreateBlobRequest(r)
	if err != nil {
		Log(r).WithError(err).Info("wrong request")
		ape.RenderErr(w, problems.BadRequest(err)...)
		return
	}

	fmt.Println(request)

	var resultBlob data.Blob
	resultBlob, err = BlobQ(r).Insert(data.Blob{
		Creator: request.Relationships.AccountData.Data.ID,
		Data:    json.RawMessage(request.Attributes.BlobData),
	})

	if err != nil {
		Log(r).WithError(err).Error("failed to insert blob")
		ape.RenderErr(w, problems.InternalError())
		return
	}

	tempKey := resources.Key{ID: resultBlob.Creator, Type: "account"}
	tempCreator := resources.Relation{Data: &tempKey}

	response := resources.BlobResponse{
		Data: newBlobModel(resultBlob, &tempCreator),
	}
	ape.Render(w, response)
}

func newBlobModel(blob data.Blob, tempCreator *resources.Relation) resources.Blob {
	return resources.Blob{
		Key:           resources.NewKeyInt64(blob.ID, resources.BLOB),
		Relationships: resources.BlobRelationships{AccountData: tempCreator},
		Attributes:    resources.BlobAttributes{BlobData: blob.Data},
	}
}
