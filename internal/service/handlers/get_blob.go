package handlers

import (
	"blob/internal/service/handlers/requests"
	"blob/resources"
	"gitlab.com/distributed_lab/ape"
	"gitlab.com/distributed_lab/ape/problems"
	"net/http"
)

func GetBlob(w http.ResponseWriter, r *http.Request) {
	request, err := requests.NewGetBlobRequest(r)
	if err != nil {
		Log(r).WithError(err).Info("wrong request")
		ape.RenderErr(w, problems.BadRequest(err)...)
		return
	}

	result, err := BlobQ(r).FilterByID(request.BlobID).Get()
	if result == nil {
		ape.Render(w, problems.NotFound())
		return
	}
	if err != nil {
		Log(r).WithError(err).Error("failed to get blob")
		ape.RenderErr(w, problems.InternalError())
		return
	}

	tempKey := resources.Key{ID: result.Creator, Type: "account"}
	tempCreator := resources.Relation{Data: &tempKey}

	response := resources.BlobResponse{
		Data: newBlobModel(*result, &tempCreator),
	}
	ape.Render(w, response)
}
