package requests

import (
	"blob/resources"
	"encoding/json"
	"gitlab.com/distributed_lab/logan/v3/errors"
	"net/http"
)

func NewCreateBlobRequest(r *http.Request) (resources.Blob, error) {
	var request resources.Blob
	err := json.NewDecoder(r.Body).Decode(&request)
	if err != nil {
		return request, errors.Wrap(err, "failed to unmarshal")
	}

	return request, nil
}
