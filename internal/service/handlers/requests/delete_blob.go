package requests

import (
	"github.com/go-chi/chi"
	"github.com/spf13/cast"
	"gitlab.com/distributed_lab/urlval"
	"net/http"
)

type DeleteBlobRequest struct {
	BlobID string `url:"-"`
}

func NewDeleteBlobRequest(r *http.Request) (DeleteBlobRequest, error) {
	request := DeleteBlobRequest{}

	err := urlval.Decode(r.URL.Query(), &request)
	if err != nil {
		return request, err
	}

	request.BlobID = cast.ToString(chi.URLParam(r, "id"))

	return request, nil
}
