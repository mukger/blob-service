package service

import (
	"blob/internal/data/postgres"
	"blob/internal/service/handlers"
	"github.com/go-chi/chi"
	"gitlab.com/distributed_lab/ape"
)

func (s *service) router() chi.Router {
	r := chi.NewRouter()

	r.Use(
		ape.RecoverMiddleware(s.log),
		ape.LoganMiddleware(s.log),
		ape.CtxMiddleware(
			handlers.CtxLog(s.log),
			handlers.CtxBlobQ(postgres.NewBlobsQ(s.cfg.DB())),
		),
	)
	r.Route("/integrations/blob", func(r chi.Router) {
		// configure endpoints here
		r.Get("/", handlers.GetBlobs)
		r.Post("/", handlers.CreateBlob)
		r.Get("/{id}", handlers.GetBlob)
		r.Delete("/{id}", handlers.DeleteBlob)
	})

	return r
}
