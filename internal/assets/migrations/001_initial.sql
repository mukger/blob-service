-- +migrate Up

create table blobs (
    id bigserial primary key,
    creator text not null,
    data jsonb not null
);

-- +migrate Down

drop table blobs;