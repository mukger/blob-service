package postgres

import (
	"blob/internal/data"
	"database/sql"
	"fmt"
	sq "github.com/Masterminds/squirrel"
	"gitlab.com/distributed_lab/kit/pgdb"
	"strconv"
	"strings"
)

const blobsTable = "blobs"

type blobsQ struct {
	db  *pgdb.DB
	sql sq.SelectBuilder
}

func NewBlobsQ(db *pgdb.DB) data.BlobQ {
	return &blobsQ{
		db:  db.Clone(),
		sql: sq.Select("n.*").From(fmt.Sprintf("%s as n", blobsTable)),
	}
}

func (q *blobsQ) New() data.BlobQ {
	return NewBlobsQ(q.db)
}

func (q *blobsQ) Transaction(fn func(q data.BlobQ) error) error {
	return q.db.Transaction(func() error {
		return fn(q)
	})
}

func (q *blobsQ) Get() (*data.Blob, error) {
	var result data.Blob
	err := q.db.Get(&result, q.sql)
	if err == sql.ErrNoRows {
		return nil, nil
	}

	return &result, err
}

func (q *blobsQ) Select() ([]data.Blob, error) {
	var result []data.Blob
	err := q.db.Select(&result, q.sql)
	return result, err
}

func (q *blobsQ) Insert(value data.Blob) (data.Blob, error) {
	var result data.Blob

	stm := sq.Insert(blobsTable).SetMap(map[string]interface{}{
		"data":    value.Data,
		"creator": value.Creator,
	}).Suffix("returning *")

	err := q.db.Get(&result, stm)
	return result, err
}

func (q *blobsQ) FilterByID(ids ...int64) data.BlobQ {
	q.sql = q.sql.Where(sq.Eq{"n.id": ids})
	return q
}

func (q *blobsQ) Delete(ids string) error {
	tempArray := strings.Split(ids, ",")
	for i := 0; i < len(tempArray); i++ {
		tempValue, err := strconv.Atoi(tempArray[i])
		stmt := sq.Delete(blobsTable).Where(sq.Eq{"id": tempValue})
		err = q.db.Exec(stmt)
		if err != nil {
			return err
		}
	}

	return nil
}

func (q *blobsQ) Page(pageParams pgdb.OffsetPageParams) {
	q.sql = pageParams.ApplyTo(q.sql, "id")
}

func (q *blobsQ) FilterByOwner(owners ...string) {
	q.sql = q.sql.Where(sq.Eq{"creator": owners})
}
