package data

import (
	"encoding/json"
	"gitlab.com/distributed_lab/kit/pgdb"
)

type BlobQ interface {
	New() BlobQ
	Get() (*Blob, error)
	Select() ([]Blob, error)
	Insert(data Blob) (Blob, error)
	FilterByID(id ...int64) BlobQ
	Delete(ids string) error
	Page(page pgdb.OffsetPageParams)
	FilterByOwner(owners ...string)
}

type Blob struct {
	ID      int64           `db:"id" structs:"-"`
	Creator string          `db:"creator" structs:"-"`
	Data    json.RawMessage `db:"data" structs:"-"`
}
