/*
 * GENERATED. Do not modify. Your changes might be overwritten!
 */

package resources

import "encoding/json"

type BlobAttributes struct {
	BlobData json.RawMessage  `json:"blob_data"`
	Data     *json.RawMessage `json:"data,omitempty"`
}
