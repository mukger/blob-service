FROM golang:1.18-alpine as buildbase

RUN apk add git build-base

WORKDIR /go/src/blob
COPY vendor .
COPY . .

RUN GOOS=linux go build  -o /usr/local/bin/blobService /go/src/blob


FROM alpine:3.9

COPY --from=buildbase /usr/local/bin/blobService /usr/local/bin/blobService
RUN apk add --no-cache ca-certificates

ENTRYPOINT ["blobService"]
